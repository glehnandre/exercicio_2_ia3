import requests
import logging

base_url = 'https://iesb-bot-app.herokuapp.com'
logger = logging.getLogger('TelegramBot')


def post_agendamento(dia, sintomas, hora, convenio):
    endpoint = '/marcacao'

    final_url = base_url + endpoint 

    obj = {'data': dia, 'hora' : hora, 'convenio' : convenio}
    logger.info('######### Agendamento: ')
    logger.info(obj)
    logger.info('######### URL do serviço: ' + final_url)
    response = requests.post(final_url, data = obj)
    logger.info('######### Response:' + response.content.decode())

    return response.content.decode()

def get_dias():
    endpoint = '/datas-disponiveis'

    final_url = base_url + endpoint
    logger.info('######### URL do serviço: ' + final_url)

    response = requests.get(final_url).json()

    logger.info('######### Response:')
    logger.info(response)

    return response

def get_horarios(dia):
    endpoint = '/horarios-disponiveis'

    parameters = '?' + '&data=' + dia

    final_url = base_url + endpoint + parameters

    logger.info('######### URL do serviço: ' + final_url)

    response = requests.get(final_url).json()

    return response
