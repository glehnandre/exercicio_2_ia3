from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson import TextToSpeechV1, SpeechToTextV1
from io import BytesIO
import os
import logging

T2S_TOKEN = os.environ.get('T2S_TOKEN')
T2S_URL = os.environ.get('T2S_URL')
S2T_TOKEN = os.environ.get('S2T_TOKEN')
S2T_URL = os.environ.get('S2T_URL')


logger = logging.getLogger('TelegramBot')

t2sauth = IAMAuthenticator('iO7Y79cBFYQ55XIob9aG47XAxgHb1nekMw6zd5zATtus')
s2tauth = IAMAuthenticator('V2RvHy2pkAgQA5nL6FjV1e36yLxEfSm94tSQIhvoiRex')

text2speech = TextToSpeechV1(
    authenticator=t2sauth
)

text2speech.set_service_url('https://api.us-south.text-to-speech.watson.cloud.ibm.com/instances/3850e848-28c2-4c91-addf-cfaf432d6cbe')

speech2text = SpeechToTextV1(
    authenticator=s2tauth
)

speech2text.set_service_url('https://api.us-south.speech-to-text.watson.cloud.ibm.com/instances/5143fa5d-ca52-4b31-b94f-0b0a0c8f7fe1')

def convert_voice(audio_file):
    response = speech2text.recognize(
        audio=audio_file,
        content_type='audio/ogg',
        model='pt-BR_NarrowbandModel'
    )
    result = response.get_result()
    logger.info('Detectada frase: ' + result['results'][0]['alternatives'][0]['transcript'])
    return result['results'][0]['alternatives'][0]['transcript']

def convert_text(message):
    response = text2speech.synthesize(
        text=message,
        voice='pt-BR_IsabelaV3Voice',
        accept='audio/ogg'
    )
    logger.info('Convertando texto em voz')
    return BytesIO(response.get_result().content)
