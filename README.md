# IESB Pós graduação - turma de IA3 

Chatbot usando telegram e aPI de serviços customizada feita em java para consulta médica. Temos as seguintes features:
* Integração com Telegram
* Integração com API de voz
* Integração com API REST (feita em java, fontes no git: )
** Pesquisa por dias e horários. Quando solicitamos um agendamento, por exemplo "quero agendar um exame", a aplicação pergunta qual o melhor dia. Nessa hora a API consulta por dias disponíveis. Quando o usuário responde o dia, a api faz uma nova pesquisa para se recuperar os horários. 
** No agendamento de consultas. Pesquise por agendamento de consulta, ou similares, responda as perguntas e o resultado é salvo na agenda fornecida pelo endpoint do serviço. Ele recupera todas as informações do passo anterior como dia, hora e convenio e passa para a API. Para demonstração, a agenda é uma tabela no banco de dados.


## Membros
* André von Glehn
* Stefania 
* Cacio Costa

## Turma tecnologias disruptivas

## OBSERVAÇÕES

O chatbot foi feito em python e está hospedada e rodando no heroku em https://iesb-app.herokuapp.com.
A API usada foi contruida em java e esta hospedada também no heroku em https://iesb-bot-app.herokuapp.com.
O bot do telegram está em @MedicoIESB_bot.



