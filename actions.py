import medicos_api
import logging

logger = logging.getLogger('TelegramBot')

def action_handler(action, parameters, return_var):
    return_values = {}
    if action == 'dias':
        return_values = get_dias(return_var)
    elif action == 'horarios':
        return_values = get_horarios(parameters, return_var)
    elif action == 'agendamento':
        return_values = post_agendamento(parameters, return_var)
    return {
            'skills': {
                'main skill': {
                    'user_defined': return_values
                }
            }
        }


def get_dias(return_var):
    dias = medicos_api.get_dias()

    retorno = '\n'

    retorno = '\n\n'.join(dias)

    return {
        return_var: retorno
    }

def get_horarios(parameters, return_var):
    data = parameters['dia']
    if data:
        logger.info('######### DATA: ' + data)

        horas = medicos_api.get_horarios(data)

        retorno = '\n\n'.join(horas)

        return {
            return_var: retorno
        }
    else: 
        return {
            return_var: 'Não há horários disponíveis.'
        }


def post_agendamento(parameters, return_var):
    dia = parameters['dia']
    sintomas = parameters['sintomas']
    hora = parameters['hora']
    convenio = parameters['convenio']

    # Fazer a chamada para o medicos_api passando os parametros. 

    agendamento = medicos_api.post_agendamento(dia, sintomas, hora, convenio)

    retorno = agendamento

    return {
        return_var: retorno
    }


